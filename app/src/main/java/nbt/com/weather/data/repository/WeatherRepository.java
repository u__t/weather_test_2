package nbt.com.weather.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import nbt.com.weather.DisposableManager;
import nbt.com.weather.WeatherDataCity;
import nbt.com.weather.data.net.Requests;
import nbt.com.weather.data.room.CitiesDAO;
import nbt.com.weather.data.room.CitiesWeatherDao;
import nbt.com.weather.data.room.CityEntity;
import nbt.com.weather.data.room.CityWeatherEntity;
import nbt.com.weather.data.room.RDBClass;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class WeatherRepository {

    private Requests mRequest;
    private CitiesDAO mCitiesDAO;
    private CitiesWeatherDao mCurrentWeatherDAO;
    private Context mContext;
    @Inject
    public WeatherRepository(Requests requests, RDBClass rdbClass, Context context) {
        Log.d("wt", " - WeatherRepository constructor - ");
        mRequest = requests;
        mCitiesDAO = rdbClass.getCitiesDAO();
        mCurrentWeatherDAO = rdbClass.getCitiesWeatherDao();
        mContext = context;

        wrightDefaultValue();
    }

    //если приложение открывается впервые запишет в бд 2 города
    private void wrightDefaultValue() {
        SharedPreferences prefs = mContext.getSharedPreferences("defaults",
                Context.MODE_PRIVATE);
        if (prefs.getBoolean("firstRun", true)) {
            insert("Moscow");
            insert("Saint Petersburg");
            prefs.edit().putBoolean("firstRun", false).apply();
        }
    }

    public LiveData<List<WeatherDataCity>> getWeatherForCities() {
        final MutableLiveData<List<WeatherDataCity>> data = new MutableLiveData<>();
        //загрузить города
        if(isOnline()) {
            DisposableManager.add(mCitiesDAO.getAllCities()
                            .subscribeOn(Schedulers.io())
                            .flatMapSingle(storedSuggestions ->
                                    Flowable.fromIterable(storedSuggestions)
                                            .map(CityEntity::getCityName)
                                            .doOnNext(CityEntity -> Log.d("wt", "Selection: " + CityEntity))
                                            .flatMap(cityName -> mRequest.getWeatherF(cityName, Requests.API_KEY, "metric"))
                                            .map(WeatherDataCity::new)
                                            .doOnNext(city -> mCurrentWeatherDAO.insert(cityWeatherToDBEntity(city)))
                                            .toList()
                            )
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnError(e -> Log.e("wt", e.getMessage()))
                            .onErrorResumeNext(s -> {
                                Log.e("wt", "onErrorResumeNext");

                            })
                            .subscribe(list -> {
                                Log.d("wt", "-------------------");
                                for (WeatherDataCity city : list) {
                                    Log.d("wt", "entity = " + city.getCityName());
                                }
                                data.setValue(list);

                            })
                    //.subscribe(data::setValue));
            );
        } else {
            DisposableManager.add(
                    mCurrentWeatherDAO.getAllCities()
                            .subscribeOn(Schedulers.io())
                            .flatMapSingle(list -> Flowable.fromIterable(list)
                                    .map(this::DBEntityToWeatherCity)
                                    .toList()
                            )
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(data::setValue)
            );
        }
        return data;
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(null != cm) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null &&
                    netInfo.isConnectedOrConnecting();
        }
        return false;
    }

    private WeatherDataCity DBEntityToWeatherCity(@NonNull CityWeatherEntity entity) {
        WeatherDataCity city = new WeatherDataCity();
        city.setCityName(entity.getName());
        city.setCurrentTemp(entity.getCurrentTemp());
        city.setId(entity.getId());
        return city;
    }

    private CityWeatherEntity cityWeatherToDBEntity(@NonNull WeatherDataCity city) {
        CityWeatherEntity result = new CityWeatherEntity();
        result.setName(city.getCityName());
        result.setCurrentTemp(city.getCurrentTemp());
        result.setId(city.getId());
        return result;
    }

    public void insert(String cityName) {
        CityEntity city = new CityEntity();
        city.setCityName(cityName);
        insert(city);
    }

    private void insert(CityEntity city) {
        new insetAsyncTask(mCitiesDAO).execute(city);
    }

    private static class insetAsyncTask extends AsyncTask<CityEntity, Void, Void> {
        private CitiesDAO mDao;
        insetAsyncTask(CitiesDAO dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(final CityEntity ... params){
            mDao.insert(params[0]);
            return null;
        }
    }
}
