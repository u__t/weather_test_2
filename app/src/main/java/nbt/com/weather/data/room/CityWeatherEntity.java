package nbt.com.weather.data.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static nbt.com.weather.data.room.CitiesWeatherDao.NAME_OF_CITY;

@Entity(tableName = CitiesWeatherDao.CITIES_WEATHER_TABLE_NAME)
public class CityWeatherEntity {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = NAME_OF_CITY)
    private String mName;

    @ColumnInfo(name = "currentTemp")
    private double mCurrentTemp;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public double getCurrentTemp() {
        return mCurrentTemp;
    }

    public void setCurrentTemp(double mCurrentTemp) {
        this.mCurrentTemp = mCurrentTemp;
    }
}
