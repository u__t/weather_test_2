package nbt.com.weather.data.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface CitiesWeatherDao {
    String CITIES_WEATHER_TABLE_NAME = "cities_weather_table";
    String NAME_OF_CITY = "name";

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CityWeatherEntity city);

    @Query("SELECT * from " + CITIES_WEATHER_TABLE_NAME + " ORDER BY " + NAME_OF_CITY + " ASC")
    Flowable<List<CityWeatherEntity>> getAllCities();
}
