package nbt.com.weather.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "weather.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String DOUBLE_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CITIES =
            "CREATE TABLE " + ContractClass.CityTable.TABLE_NAME + " (" +
                    //ContractClass.CityTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ContractClass.CityTable.CITY_NAME + " TEXT PRIMARY KEY" + COMMA_SEP +
                    ContractClass.CityTable.CURRENT_TEMP + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.CityTable.CURRENT_TEMP_MIN + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.CityTable.CURRENT_TEMP_MAX + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.CityTable.CURRENT_PRESSURE + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.CityTable.CURRENT_HUMIDITY + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.CityTable.WIND_SPEED + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.CityTable.DATE + TEXT_TYPE +
                    " )";

    private static final String SQL_CREATE_FORECAST =
            "CREATE TABLE " + ContractClass.ForecastTable.TABLE_NAME + " (" +
                    //ContractClass.ForecastTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ContractClass.ForecastTable.CITY_NAME + TEXT_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.CURRENT_TEMP + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.CURRENT_TEMP_MIN + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.CURRENT_TEMP_MAX + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.CURRENT_PRESSURE + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.CURRENT_HUMIDITY + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.WIND_SPEED + DOUBLE_TYPE + COMMA_SEP +
                    ContractClass.ForecastTable.DATE + TEXT_TYPE +
                    " )";

    static public boolean isDatabaseExist(Context context) {
        File dbFile = context.getDatabasePath(DATABASE_NAME);
        return dbFile.exists();
    }

    private static final String SQL_DELETE_CITIES =
            "DROP TABLE IF EXISTS " + ContractClass.CityTable.TABLE_NAME;

    private static final String SQL_DELETE_FORECAST =
            "DROP TABLE IF EXISTS " + ContractClass.ForecastTable.TABLE_NAME;

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CITIES);
        db.execSQL(SQL_CREATE_FORECAST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CITIES);
        db.execSQL(SQL_DELETE_FORECAST);
        onCreate(db);
    }
}
