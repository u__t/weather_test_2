package nbt.com.weather.data.service;

import android.content.Context;
import android.content.Intent;

/**
 * создаёт интент и запускает сервис
 * */
public class ServiceHelper {
    public static final String CITY_NAME_KEY = "city_name_key";

    private static ServiceHelper mInstance;

    public static ServiceHelper getInstance() {
        if (null == mInstance) {
            mInstance = new ServiceHelper();
        }
        return mInstance;
    }

    private ServiceHelper() {

    }

    public void loadCity(Context context, String cityName) {
        try {
            Intent intent = new Intent(context, LoadService.class);
            intent.putExtra(CITY_NAME_KEY, cityName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
