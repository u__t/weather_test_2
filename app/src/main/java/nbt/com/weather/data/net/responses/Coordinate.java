package nbt.com.weather.data.net.responses;

import com.google.gson.annotations.SerializedName;

public class Coordinate {
    @SerializedName("lon")
    public double longitude;

    @SerializedName("lat")
    public double latitude;
}
