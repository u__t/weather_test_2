package nbt.com.weather.data.net;

import io.reactivex.Flowable;
import nbt.com.weather.data.net.responses.CurrentWeatherData;
import retrofit2.http.GET;
import retrofit2.http.Query;
import io.reactivex.Observable;

public interface Requests {

    String BASE_URL = "http://api.openweathermap.org";
    String API_KEY = "fb0cf5f7724f5ca498ff7236efa54041";

    @GET("data/2.5/weather")
    Observable<CurrentWeatherData> getWeather(@Query("q") String query,
                                               @Query("appid") String appid,
                                               @Query("units") String metric);

    @GET("data/2.5/weather")
    Flowable<CurrentWeatherData> getWeatherF(@Query("q") String query,
                                             @Query("appid") String appid,
                                             @Query("units") String metric);
}
