package nbt.com.weather.data.net.responses;

import com.google.gson.annotations.SerializedName;

public class MainResponse {
    @SerializedName("temp")
    public double mTemperature;

    @SerializedName("pressure")
    public double mPressure;

    @SerializedName("humidity")
    public double mHumidity;

    @SerializedName("temp_min")
    public double mTemperatureMin;

    @SerializedName("temp_max")
    public double mTemperatureMax;
}
