package nbt.com.weather.data.interfaces;

import javax.inject.Singleton;

import dagger.Component;
import nbt.com.weather.data.repository.WeatherRepository;
import nbt.com.weather.modules.ContextModule;
import nbt.com.weather.modules.RepositoryModule;

@Singleton
@Component(modules = {RepositoryModule.class, ContextModule.class})
public interface RepositoryComponent {
    WeatherRepository getRepository();
}
