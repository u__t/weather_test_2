package nbt.com.weather.data.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class LoadService extends Service {

    @Override
    public void onCreate() {
        Log.d("wt", "-onCreate-");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("wt", "-onStartCommand-");
        if (null != intent && null != intent.getExtras()) {
            String cityName = intent.getExtras().getString(ServiceHelper.CITY_NAME_KEY);
            makeQuery(cityName);
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d("wt", "-onDestroy-");
    }

    //todo сделать запрос
    private void makeQuery(String cityName) {
        try {
            //rxJava

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
