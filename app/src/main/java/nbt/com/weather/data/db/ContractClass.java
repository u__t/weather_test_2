package nbt.com.weather.data.db;

import android.provider.BaseColumns;

public final class ContractClass {
    public ContractClass() {}

    public static abstract class CityTable implements BaseColumns {
        public static final String TABLE_NAME = "city_table";
        public static final String _ID = "id";
        public static final String CITY_NAME = "city_name";
        public static final String CURRENT_TEMP = "current_temp";
        public static final String CURRENT_TEMP_MIN = "current_temp_min";
        public static final String CURRENT_TEMP_MAX = "current_temp_max";
        public static final String CURRENT_PRESSURE = "current_pressure";
        public static final String CURRENT_HUMIDITY = "current_humidity";
        public static final String WIND_SPEED = "wind_speed";
        public static final String DATE = "date";
    }

    public static abstract class ForecastTable implements BaseColumns {
        public static final String TABLE_NAME = "forecast_table";
        public static final String _ID = "id";
        public static final String CITY_NAME = "city_name";
        public static final String CURRENT_TEMP = "current_temp";
        public static final String CURRENT_TEMP_MIN = "current_temp_min";
        public static final String CURRENT_TEMP_MAX = "current_temp_max";
        public static final String CURRENT_PRESSURE = "current_pressure";
        public static final String CURRENT_HUMIDITY = "current_humidity";
        public static final String WIND_SPEED = "wind_speed";
        public static final String DATE = "date";
    }
}
