package nbt.com.weather.data.net.responses;

import com.google.gson.annotations.SerializedName;

public class Weather {
    @SerializedName("id")
    public int mId;

    @SerializedName("main")
    public String mMain;

    @SerializedName("description")
    public String mDescription;

    @SerializedName("icon")
    public String mIcon;
}
