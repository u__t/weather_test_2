package nbt.com.weather.data.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = CitiesDAO.CITIES_TABLE_NAME)
public class CityEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @NonNull
    @ColumnInfo(name = CitiesDAO.CITY_NAME)
    private String mCityName;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    @NonNull
    public String getCityName() {
        return mCityName;
    }

    public void setCityName(@NonNull String mCityName) {
        this.mCityName = mCityName;
    }
}
