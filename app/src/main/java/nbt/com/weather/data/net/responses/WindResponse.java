package nbt.com.weather.data.net.responses;

import com.google.gson.annotations.SerializedName;

public class WindResponse {
    @SerializedName("speed")
    public double mSpeed;

    @SerializedName("deg")
    public double mDegree;
}
