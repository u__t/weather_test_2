package nbt.com.weather.data.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface CitiesDAO {

    String CITIES_TABLE_NAME = "cities_table";
    String CITY_NAME = "cityName";

    @Insert
    void insert(CityEntity city);

    @Delete
    void delete(CityEntity city);

    @Query("SELECT * from " + CITIES_TABLE_NAME + " ORDER BY " + CITY_NAME + " ASC")
    Flowable<List<CityEntity>> getAllCities();
}
