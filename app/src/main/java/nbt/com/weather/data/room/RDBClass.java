package nbt.com.weather.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {CityEntity.class, CityWeatherEntity.class},  version = 1)
public abstract class RDBClass extends RoomDatabase {
    public abstract CitiesDAO getCitiesDAO();
    public abstract CitiesWeatherDao getCitiesWeatherDao();
}
