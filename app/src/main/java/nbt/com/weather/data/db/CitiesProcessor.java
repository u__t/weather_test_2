package nbt.com.weather.data.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import nbt.com.weather.WeatherDataCity;

public class CitiesProcessor {
    @SuppressLint("InlinedApi")
    private static final String SELECTION = ContractClass.CityTable.CITY_NAME + " =?";
    //todo добавить горд
    public static long saveToDB(Context context, WeatherDataCity data){
        try {
            Log.d("wt", "-saveToDB-");
            DBHelper dbHelper = new DBHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(ContractClass.CityTable.CITY_NAME, data.getCityName());
            values.put(ContractClass.CityTable.CURRENT_TEMP, data.getCurrentTemp());
            values.put(ContractClass.CityTable.CURRENT_TEMP_MIN, data.getCurrentTempMin());
            values.put(ContractClass.CityTable.CURRENT_TEMP_MAX, data.getCurrentTempMax());
            values.put(ContractClass.CityTable.CURRENT_PRESSURE, data.getCurPressure());
            values.put(ContractClass.CityTable.CURRENT_HUMIDITY, data.getCurrHumidity());
            values.put(ContractClass.CityTable.WIND_SPEED, data.getWindSpeed());
            values.put(ContractClass.CityTable.DATE, data.getDate());

            long newRowId;
            newRowId = db.insertWithOnConflict(
                    ContractClass.CityTable.TABLE_NAME,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_IGNORE);

            Log.d("wt", "newRowId = " + newRowId);
            return newRowId;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    //todo прочитать город
    public static WeatherDataCity readsFromDB(Context context, String cityName){
        try {
            Log.d("wt", "-readsFromDB-");
            if(DBHelper.isDatabaseExist(context)){
                WeatherDataCity result;

                DBHelper dbHelper = new DBHelper(context);
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                Cursor cursor = db.query(
                        ContractClass.CityTable.TABLE_NAME,
                        null,
                        SELECTION,
                        new String[]{cityName},
                        null,
                        null,
                        null
                );
                if(null != cursor) {
                    cursor.moveToFirst();
                    result = readFromCursor(cursor);
                    cursor.close();
                    return result;
                } else
                    return null;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static WeatherDataCity readFromCursor(Cursor cursor){
        try {
            String name = cursor.getString(cursor.getColumnIndexOrThrow(ContractClass.CityTable.CITY_NAME));
            double temperature = cursor.getDouble(cursor.getColumnIndexOrThrow(ContractClass.CityTable.CURRENT_TEMP));
            double temperatureMin = cursor.getDouble(cursor.getColumnIndexOrThrow(ContractClass.CityTable.CURRENT_TEMP_MIN));
            double temperatureMax = cursor.getDouble(cursor.getColumnIndexOrThrow(ContractClass.CityTable.CURRENT_TEMP_MAX));
            double pressure = cursor.getDouble(cursor.getColumnIndexOrThrow(ContractClass.CityTable.CURRENT_PRESSURE));
            double humidity = cursor.getDouble(cursor.getColumnIndexOrThrow(ContractClass.CityTable.CURRENT_HUMIDITY));
            double windSpeed = cursor.getDouble(cursor.getColumnIndexOrThrow(ContractClass.CityTable.WIND_SPEED));
            String date = cursor.getString(cursor.getColumnIndexOrThrow(ContractClass.CityTable.DATE));

            WeatherDataCity dataCity = new WeatherDataCity();
            dataCity.setCityName(name);
            dataCity.setCurrentTemp(temperature);
            dataCity.setCurrentTempMin(temperatureMin);
            dataCity.setCurrentTempMax(temperatureMax);
            dataCity.setCurPressure(pressure);
            dataCity.setCurrHumidity(humidity);
            dataCity.setWindSpeed(windSpeed);
            dataCity.setDate(date);

            return dataCity;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //todo обновить город
    public static int updateCity(Context context, WeatherDataCity cityData) {

        Log.d("wt", "-updateCity-");

        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ContractClass.CityTable.CITY_NAME, cityData.getCityName());
        values.put(ContractClass.CityTable.CURRENT_TEMP, cityData.getCurrentTemp());
        values.put(ContractClass.CityTable.CURRENT_TEMP_MIN, cityData.getCurrentTempMin());
        values.put(ContractClass.CityTable.CURRENT_TEMP_MAX, cityData.getCurrentTempMax());
        values.put(ContractClass.CityTable.CURRENT_PRESSURE, cityData.getCurPressure());
        values.put(ContractClass.CityTable.CURRENT_HUMIDITY, cityData.getCurrHumidity());
        values.put(ContractClass.CityTable.WIND_SPEED, cityData.getWindSpeed());
        values.put(ContractClass.CityTable.DATE, cityData.getDate());

        return db.update(ContractClass.CityTable.TABLE_NAME,
                values,
                ContractClass.CityTable.CITY_NAME + "=?",
                new String[] { String.valueOf(cityData.getCityName())}
        );
    }

    //todo удалить город
    public static void removeCity(Context context, WeatherDataCity cityData) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(ContractClass.CityTable.TABLE_NAME,
                ContractClass.CityTable.CITY_NAME+ "=?",
                new String[] { String.valueOf(cityData.getCityName())}
        );
    }
}
