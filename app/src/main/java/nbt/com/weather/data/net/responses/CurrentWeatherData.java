package nbt.com.weather.data.net.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CurrentWeatherData {
    @SerializedName("coord")
    public Coordinate mCoordinate;

    @SerializedName("weather")
    public ArrayList<Weather> mWeather;

    @SerializedName("base")
    public String mBase;

    @SerializedName("main")
    public MainResponse mMain;

    @SerializedName("visibility")
    public double mVisibility;

    @SerializedName("wind")
    public WindResponse mWind;

    @SerializedName("clouds")
    public CloudsResponse mClouds;

    @SerializedName("id")
    public int mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("cod")
    public int mCode;

    @SerializedName("dt_txt")
    public String mDate;

    public String toString() {
        return mName;
    }
}
