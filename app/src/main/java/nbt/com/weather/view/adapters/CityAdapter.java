package nbt.com.weather.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nbt.com.weather.R;
import nbt.com.weather.WeatherDataCity;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private List<WeatherDataCity> mData;

    public CityAdapter(@NonNull List<WeatherDataCity> cities){
        mData = cities;
    }

    @Override
    public int getItemCount(){
        return (null == mData) ? 0 : mData.size();
    }

    @NonNull
    @Override
    public CityAdapter.CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_holder_city, parent, false);
        return new CityAdapter.CityViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        try {
            WeatherDataCity element = mData.get(position);
            if(null != element) {
                holder.mCityNameView.setText(String.valueOf(element.getCityName()));
                holder.mWeatherView.setText(String.valueOf(element.getCurrentTemp()));
                holder.mCity = element;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----------------------------------------------------------------------------------------------
    class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mCityNameView;
        private TextView mWeatherView;
        private WeatherDataCity mCity;
        public CityViewHolder(View view){
            super(view);
            mCityNameView = view.findViewById(R.id.city_name_id);
            mWeatherView = view.findViewById(R.id.weather_id);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
