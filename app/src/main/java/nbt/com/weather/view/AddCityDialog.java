package nbt.com.weather.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import nbt.com.weather.R;
import nbt.com.weather.view_model.WeatherViewModel;

public class AddCityDialog extends DialogFragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_dialog, container);

        setOkBtn(v);
        setCancelBtn(v);
        return v;
    }

    private void setOkBtn(View v) {
        try {
            Button btn = v.findViewById(R.id.btn_ok_id);
            final EditText editText = v.findViewById(R.id.add_city_id);
            if(null != btn){
                btn.setOnClickListener(view -> {
                    Log.d("wt", "--ok--");
                    if(null != editText) {
                        String cityName = editText.getText().toString();
                        if(!cityName.isEmpty() && null != getActivity()) {
                            WeatherViewModel viewModel = ViewModelProviders.of(getActivity())
                                    .get(WeatherViewModel.class);
                            viewModel.setNewCity(cityName);
                        }
                    }
                    dismiss();
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCancelBtn(View v) {
        try {
            Button btn = v.findViewById(R.id.btn_cancel_id);
            if(null != btn){
                btn.setOnClickListener(view -> {
                    Log.d("wt", "--cancel--");
                    dismiss();
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d("wt", "onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d("wt", "onCancel");
    }
}
