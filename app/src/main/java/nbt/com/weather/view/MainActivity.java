package nbt.com.weather.view;

import android.arch.lifecycle.ViewModelProviders;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import nbt.com.weather.R;
import nbt.com.weather.WeatherDataCity;
import nbt.com.weather.view.adapters.CityAdapter;
import nbt.com.weather.view_model.WeatherViewModel;


public class MainActivity extends AppCompatActivity {

    private WeatherViewModel mViewModel;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("mLog", "-onCreate-");
        //------------------------------------------------------------------------------------------
        mRecyclerView = findViewById(R.id.recycler_view_id);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        //-----------------------------------view model---------------------------------------------
        mViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        mViewModel.getCityInfo().observe(this, citiesWeatherList -> {
            Log.d("wt", "-- it work --");
            setAdapter(citiesWeatherList);
        });
        //------------------------------------------------------------------------------------------
        setAddBtn();
    }

    private void setAdapter(List<WeatherDataCity> cities) {
        if(null != cities) {
            //todo не создавать каждый раз новый адаптер
            mAdapter = new CityAdapter(cities);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private void setAddBtn() {
        try {
            FloatingActionButton fab = findViewById(R.id.add_fab_id);
            if(null != fab) {
                fab.setOnClickListener(view -> startAddDialog());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startAddDialog(){
        try {
            AddCityDialog dialog = new AddCityDialog();
            dialog.show(getSupportFragmentManager(), "addDialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
