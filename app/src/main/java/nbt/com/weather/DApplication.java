package nbt.com.weather;

import android.app.Application;

import nbt.com.weather.data.interfaces.DaggerRepositoryComponent;
import nbt.com.weather.data.interfaces.RepositoryComponent;
import nbt.com.weather.modules.ContextModule;
import nbt.com.weather.modules.RepositoryModule;

public class DApplication extends Application {

    static RepositoryComponent mComponent;

    static public RepositoryComponent getComponent() {
        return mComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerRepositoryComponent
                .builder()
                .repositoryModule(new RepositoryModule())
                .contextModule(new ContextModule(this))
                .build();
    }
}
