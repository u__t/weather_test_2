package nbt.com.weather.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nbt.com.weather.data.net.Requests;
import nbt.com.weather.data.repository.WeatherRepository;
import nbt.com.weather.data.room.RDBClass;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module(includes = ContextModule.class)
public class RepositoryModule {
    @Provides
    public WeatherRepository weatherRepository(Retrofit retrofit, RDBClass rdbClass, Context context) {
        return new WeatherRepository(retrofit.create(Requests.class), rdbClass, context);
    }

    @Provides
    public Retrofit retrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(Requests.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
    }

    @Provides
    public OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient()
                .newBuilder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    public HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    public RDBClass roomDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                RDBClass.class, "room_database")
                .build();
    }
}
