package nbt.com.weather;

import android.util.Log;

import nbt.com.weather.data.net.responses.CurrentWeatherData;

public class WeatherDataCity {
    private String mCityName;
    private double mCurrentTemp;
    private double mCurrentTempMin;
    private double mCurrentTempMax;
    private double mCurPressure;
    private double mCurrHumidity;
    private double mWindSpeed;
    private String mDate;
    private int mId;

    public WeatherDataCity() {}

    public WeatherDataCity(CurrentWeatherData data) {
        createFrom(data);
    }

    public void createFrom(CurrentWeatherData data) {
        try {
            if(null != data) {
                mCurrentTemp = data.mMain.mTemperature;
                mCurPressure = data.mMain.mPressure;
                mCurrHumidity = data.mMain.mHumidity;
                mCurrentTempMin = data.mMain.mTemperatureMin;
                mCurrentTempMax = data.mMain.mTemperatureMax;
                mWindSpeed = data.mWind.mSpeed;
                mDate = data.mDate;

                mCityName = data.mName;

                mId = data.mId;
            } else {
                Log.d("wt", "null response");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double getCurrentTemp() {
        return mCurrentTemp;
    }

    public double getCurrentTempMin() {
        return mCurrentTempMin;
    }

    public double getWindSpeed() {
        return mWindSpeed;
    }

    public double getCurrentTempMax() {
        return mCurrentTempMax;
    }

    public double getCurPressure() {
        return mCurPressure;
    }

    public double getCurrHumidity() {
        return mCurrHumidity;
    }

    public void setCurrentTemp(double mCurrentTemp) {
        this.mCurrentTemp = mCurrentTemp;
    }

    public void setCurrentTempMin(double mCurrentTempMin) {
        this.mCurrentTempMin = mCurrentTempMin;
    }

    public void setCurrentTempMax(double mCurrentTempMax) {
        this.mCurrentTempMax = mCurrentTempMax;
    }

    public void setCurPressure(double mCurPressure) {
        this.mCurPressure = mCurPressure;
    }

    public void setCurrHumidity(double mCurrHumidity) {
        this.mCurrHumidity = mCurrHumidity;
    }

    public void setWindSpeed(double mWindSpeed) {
        this.mWindSpeed = mWindSpeed;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String mCityName) {
        this.mCityName = mCityName;
    }

    public String getDate() {
        return this.mDate;
    }

    public int getId() {
        return this.mId;
    }

    public void setId(int id) {
        this.mId = id;
    }
}
