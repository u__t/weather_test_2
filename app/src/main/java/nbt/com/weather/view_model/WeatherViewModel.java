package nbt.com.weather.view_model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.List;

import nbt.com.weather.DApplication;
import nbt.com.weather.WeatherDataCity;
import nbt.com.weather.data.repository.WeatherRepository;

public class WeatherViewModel extends ViewModel {
    private LiveData<List<WeatherDataCity>> mCityInfo;

    private WeatherRepository mRepository;
    //private RepositoryComponent mComponent;


    WeatherViewModel() {
        Log.d("wt", "-WeatherViewModel-");

        initWeather();
    }

    private void initWeather() {
        Log.d("wt", "-initWeather-");
        mRepository = DApplication.getComponent().getRepository();
        mCityInfo = mRepository.getWeatherForCities();
        Log.d("wt", "-initWeather 1-");
    }

    public LiveData<List<WeatherDataCity>> getCityInfo() {
        return mCityInfo;
    }

    public void setNewCity(String cityName){
        DApplication.getComponent().getRepository().insert(cityName);
    }
}
